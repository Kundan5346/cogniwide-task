//
//  LoginVc.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class LoginVc: UIViewController {

    @IBOutlet weak var scView: UIScrollView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    private var loginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSubmit.isEnabled = false
        btnSubmit.alpha = 0.5
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if let strMsg = loginViewModel.validateCredential(tfEmail.text!, tfPassword.text!) {
            AlertView.alert("", strMsg)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostVc") as! PostVc
            SceneDelegate.shared?.window?.rootViewController = vc
            SceneDelegate.shared?.window?.makeKeyAndVisible()
        }
    }
    
}

extension LoginVc: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if tfEmail.text!.isEmail && tfPassword.text!.count > 8 {
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1
        } else {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5
        }
        
        if textField == tfEmail {
            return (textField.text!.count > 30) ? false : true
        } else if textField == tfPassword {
            return (textField.text!.count > 15) ? false : true
        }
        
        return true
    }
    
}
