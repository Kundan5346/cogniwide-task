//
//  PostVc.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class PostVc: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var arrPost = [PostDataModel]()
    var arrFavPost = [PostDataModel]()
    private var postModel = PostModel()
    var isFavPost = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postModel.postController = self
        postModel.checkForNetwork()
        tblView.tableFooterView = UIView()
        tblView.register(UINib(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
    }
    
    @IBAction func actionSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            isFavPost = false
        } else {
            isFavPost = true
        }
        self.getFavPosts()
    }
    
    func reloadData(_ arr: [[String:Any]]) {
        for i in 0..<arr.count {
            let dict = arr[i]
            arrPost.append(PostDataModel(dict: dict))
        }
        getFavPosts()
    }
    
    func getFavPosts() {
        LocalDB().getFavPost { (arr) in
            self.arrFavPost = arr
            for ob in arr {
                _=self.arrPost.filter({$0.id == ob.id}).first?.isFav = true
            }
        }
        self.tblView.reloadData()
    }
    
}

extension PostVc: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isFavPost) ? arrFavPost.count : arrPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        let ob = (isFavPost) ? arrFavPost[indexPath.row] : arrPost[indexPath.row]
        cell.configure(ob: ob)
        cell.actionFav = {
            if self.isFavPost {
                LocalDB().deleteFavPost(ob: ob)
                self.arrFavPost.remove(at: indexPath.row)
                self.tblView.reloadData()
            } else {
                if ob.isFav {
                    ob.isFav = false
                    LocalDB().deleteFavPost(ob: ob)
                } else {
                    ob.isFav = true
                    LocalDB().saveNewFavPost(ob: ob)
                }
                cell.configure(ob: ob)
            }
        }
        return cell
    }
}
