//
//  Model.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class PostDataModel {
    
    var body = ""
    var id = ""
    var title = ""
    var userId = ""
    var isFav = false

    init(dict: [String:Any]) {
        self.body = string(dict, "body")
        self.id = string(dict, "id")
        self.title = string(dict, "title")
        self.userId = string(dict, "userId")
    }
    
    init() {
        
    }
    
}
