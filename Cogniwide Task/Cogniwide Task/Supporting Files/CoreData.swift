//
//  CoreData.swift
//  Globussoft Task
//
//  Created by SMT on 19/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit
import CoreData

class LocalDB: NSObject {

    let context = kAppDelegate.persistentContainer.viewContext
    
    
    func saveNewFavPost(ob: PostDataModel) {
        let entity = NSEntityDescription.entity(forEntityName: "FavPost", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        newUser.setValue(ob.userId, forKey: "userId")
        newUser.setValue(ob.title, forKey: "title")
        newUser.setValue(ob.id, forKey: "id")
        newUser.setValue(ob.body, forKey: "body")
        newUser.setValue(true, forKey: "isFav")

        print("Storing Data..")
        do {
            try context.save()
        } catch {
            print("Storing data Failed")
        }
    }
    
    func deleteFavPost(ob: PostDataModel) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FavPost")
        request.predicate = NSPredicate(format: "%K == %@", "id", ob.id)

        let objects = try! context.fetch(request)
        for obj in objects as! [NSManagedObject] {
            context.delete(obj)
        }
        print("Saving Data..")
        do {
            try context.save()
        } catch {
           print("Saving data Failed")
        }
    }
    
    func getFavPost(completion: @escaping ([PostDataModel]) -> Swift.Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FavPost")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            
            if let result = result as? [NSManagedObject] {
                var arr = [PostDataModel]()
                for data in result {
                    let ob = PostDataModel()
                    ob.userId = data.value(forKey: "userId") as! String
                    ob.title = data.value(forKey: "title") as! String
                    ob.id = data.value(forKey: "id") as! String
                    ob.body = data.value(forKey: "body") as! String
                    ob.isFav = true
                    
                    arr.append(ob)
                }
                completion(arr)
            }
        } catch {
            print("Fetching data Failed")
        }
    }
}


