//
//  Messages.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class AlertMsg: NSObject {
    static let blankEmail = "Please enter email address."
    static let invalidEmail = "Please enter valid email address."
    static let blankPassword = "Please enter password."
    static let passwordLength = "Password size must be between 8 - 15 characters."
}

