//
//  PostCell.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    
    var actionFav: (()->Void)?
    
    func configure(ob: PostDataModel) {
        lblTitle.text = ob.title
        if ob.isFav {
            btnFav.tintColor = UIColor.red
        } else {
            btnFav.tintColor = UIColor.lightGray
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionFav(_ sender: Any) {
        self.actionFav?()
    }
}
