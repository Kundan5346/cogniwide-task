//
//  LoginViewModel.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class LoginViewModel: NSObject {

    var email = ""
    var password = ""
    
    func validateCredential(_ email: String, _ password: String) -> String? {
        if email.isEmpty {
            return AlertMsg.blankEmail
        } else if !email.isEmail {
            return AlertMsg.invalidEmail
        } else if password.isEmpty {
            return AlertMsg.blankPassword
        } else if password.count < 8 {
            return AlertMsg.passwordLength
        } else {
            self.email = email
            self.password = password
        }
        return nil
    }
    
    
}
