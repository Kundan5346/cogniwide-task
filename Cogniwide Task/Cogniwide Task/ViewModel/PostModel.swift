//
//  PostModel.swift
//  Cogniwide Task
//
//  Created by SMT on 21/02/21.
//  Copyright © 2021 MAC. All rights reserved.
//

import UIKit

class PostModel: NSObject {

    var postController: PostVc? = nil
    
    func checkForNetwork() {
        if Reachability.isConnectedToNetwork(){
            wsGetPost()
        } else {
            retrieveFromJsonFile()
        }
    }
    
    func wsGetPost() {

        var request = URLRequest(url: URL(string: "https://jsonplaceholder.typicode.com/posts")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! [[String:Any]]
                print("json-\(json)-")
                DispatchQueue.main.async {
                    self.postController?.reloadData(json)
                    self.saveToJsonFile(json)
                }
            } catch {
                print("error")
                AlertView.alert("", error.localizedDescription)
            }
        })
        task.resume()
    }
    
    func saveToJsonFile(_ postArray: [[String:Any]]) {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Post.json")

        do {
            let data = try JSONSerialization.data(withJSONObject: postArray, options: [])
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    
    
    func retrieveFromJsonFile() {

        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("Post.json")

        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            guard let postArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else { return }
            postController?.reloadData(postArray)
        } catch {
            print(error)
        }
    }
}
